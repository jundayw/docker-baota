
##创建镜像
`docker build -t {repository:tag} {path:Dockerfile}`

`docker build -t centos:7.1 ./centos`

##启动容器
`docker run -it -p 8888:80 --name {name} {repository:tag}`

`docker run -it -p 8888:80 --name centos centos:7.1`

##容器在后台运行
`docker run -d -p 8888:80 --name {name} {repository:tag}`

`docker run -d -p 8888:80 --name centos centos:7.1`

##进入容器
`docker exec -it {container-name/container-id} /bin/bash`

`docker exec -it centos /bin/bash`

##镜像列表
`docker images`

##容器列表
`docker ps -a`

##启动已停止容器
`docker start {container-name/container-id}`

##停止已运行容器
`docker stop {container-name/container-id}`

##删除镜像
`docker image rm -f {repository:tag}`

`docker image rm -f centos:7.1`

##删除容器
`docker rm -f {name}`

`docker rm -f centos`

##容器日志
`docker logs {container-name/container-id}`